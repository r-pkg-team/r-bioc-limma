r-bioc-limma (3.62.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 17 Jan 2025 00:03:23 +0100

r-bioc-limma (3.62.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 11 Jan 2025 17:49:35 +0100

r-bioc-limma (3.62.1+dfsg-1) experimental; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 08 Nov 2024 10:43:30 +0100

r-bioc-limma (3.60.6+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Charles Plessy <plessy@debian.org>  Mon, 28 Oct 2024 14:38:12 +0900

r-bioc-limma (3.60.4+dfsg-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 16 Aug 2024 13:37:54 +0200

r-bioc-limma (3.60.4+dfsg-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 01 Aug 2024 10:40:48 +0200

r-bioc-limma (3.60.4+dfsg-1~0exp0) experimental; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 30 Jul 2024 20:46:33 +0200

r-bioc-limma (3.60.3+dfsg-1~0exp) experimental; urgency=medium

  * Team upload
  * New upstream version
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 09 Jul 2024 22:07:48 +0200

r-bioc-limma (3.58.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 29 Nov 2023 21:04:33 +0100

r-bioc-limma (3.56.2+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 20 Jul 2023 16:21:55 +0200

r-bioc-limma (3.54.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 01 Feb 2023 11:29:31 +0100

r-bioc-limma (3.54.0+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 20 Nov 2022 10:52:08 +0100

r-bioc-limma (3.52.4+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 04 Oct 2022 20:06:59 +0200

r-bioc-limma (3.52.3+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 16 Sep 2022 07:23:03 +0200

r-bioc-limma (3.52.2+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 23 Jun 2022 10:43:45 +0200

r-bioc-limma (3.52.1+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 25 May 2022 10:06:59 +0200

r-bioc-limma (3.52.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 12 May 2022 12:25:32 +0200

r-bioc-limma (3.50.1+dfsg-2) unstable; urgency=medium

  * Provide autopkgtest-pkg-r.conf to make sure Test-Depends will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Tue, 08 Mar 2022 14:32:13 +0100

r-bioc-limma (3.50.1+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Tue, 08 Mar 2022 12:23:48 +0100

r-bioc-limma (3.50.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Contact.
  * Remove useless debian/source/include-binaries

 -- Andreas Tille <tille@debian.org>  Wed, 24 Nov 2021 11:09:01 +0100

r-bioc-limma (3.48.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 01 Sep 2021 20:23:47 +0200

r-bioc-limma (3.46.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Sun, 01 Nov 2020 10:38:28 +0100

r-bioc-limma (3.44.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Mon, 29 Jun 2020 16:07:02 +0200

r-bioc-limma (3.44.2+dfsg-2) unstable; urgency=medium

  * Added locfit as a build dependency.
  * Added myself as uploader.

 -- Steffen Moeller <moeller@debian.org>  Sat, 13 Jun 2020 17:47:10 +0200

r-bioc-limma (3.44.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Steffen Moeller <moeller@debian.org>  Sat, 13 Jun 2020 16:03:51 +0200

r-bioc-limma (3.44.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Fri, 15 May 2020 12:33:01 +0200

r-bioc-limma (3.42.2+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Add r-bioc-org.hs.eg.db to test-deps

 -- Dylan Aïssi <daissi@debian.org>  Tue, 25 Feb 2020 07:56:27 +0100

r-bioc-limma (3.42.2+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Steffen Moeller ]
  * r-bioc-go.db is now in the distribution, deactivated patch
    that circumvented testing against it.
  * Added a bit to the description.

  [ Dylan Aïssi ]
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Sat, 08 Feb 2020 07:27:52 +0100

r-bioc-limma (3.42.1+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g (routine-update)
  * Set field Upstream-Name in debian/copyright.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Steffen Moeller <moeller@debian.org>  Sun, 02 Feb 2020 21:25:20 +0100

r-bioc-limma (3.42.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Fixed debian/watch for BioConductor
  * Standards-Version: 4.4.1

 -- Dylan Aïssi <daissi@debian.org>  Sun, 10 Nov 2019 17:03:32 +0100

r-bioc-limma (3.40.6+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0

 -- Andreas Tille <tille@debian.org>  Sun, 28 Jul 2019 00:52:28 +0200

r-bioc-limma (3.40.2+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0
  * rename debian/tests/control.autodep8 to debian/tests/control

 -- Andreas Tille <tille@debian.org>  Thu, 04 Jul 2019 12:56:44 +0200

r-bioc-limma (3.38.3+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Add Testsuite: autopkgtest-pkg-r

 -- Dylan Aïssi <daissi@debian.org>  Sat, 08 Dec 2018 08:50:27 +0100

r-bioc-limma (3.38.2+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Refresh patches

 -- Dylan Aïssi <daissi@debian.org>  Thu, 08 Nov 2018 22:06:20 +0100

r-bioc-limma (3.36.5+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 25 Sep 2018 11:04:18 +0200

r-bioc-limma (3.36.3+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Tue, 28 Aug 2018 17:03:53 +0200

r-bioc-limma (3.36.2+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 25 Jun 2018 08:30:23 +0200

r-bioc-limma (3.36.1+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 08 May 2018 09:05:15 +0200

r-bioc-limma (3.36.0+dfsg-2) unstable; urgency=medium

  * Fix Maintainer and Vcs-fields

 -- Andreas Tille <tille@debian.org>  Sun, 06 May 2018 08:34:57 +0200

r-bioc-limma (3.36.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * dh-update-R to update Build-Depends
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Fri, 04 May 2018 12:05:04 +0200

r-bioc-limma (3.34.9+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.3
  * debhelper 11
  * d/rules: do not parse d/changelog

 -- Andreas Tille <tille@debian.org>  Mon, 12 Mar 2018 19:05:18 +0100

r-bioc-limma (3.34.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Add registry entry

 -- Andreas Tille <tille@debian.org>  Fri, 10 Nov 2017 16:26:57 +0100

r-bioc-limma (3.32.10+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Secure URI in watch file
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Sun, 22 Oct 2017 19:06:21 +0200

r-bioc-limma (3.32.7+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Bumped policy to 4.1.0 (no changes required)

 -- Steffen Moeller <moeller@debian.org>  Mon, 25 Sep 2017 23:04:35 +0200

r-bioc-limma (3.30.8+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 20 Jan 2017 09:15:01 +0100

r-bioc-limma (3.30.7+dfsg-2) unstable; urgency=medium

  * Add missing test depends: r-cran-statmod

 -- Andreas Tille <tille@debian.org>  Sun, 15 Jan 2017 14:27:44 +0100

r-bioc-limma (3.30.7+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 05 Jan 2017 15:32:13 +0100

r-bioc-limma (3.30.6+dfsg-1) unstable; urgency=medium

  [ Kevin Murray ]
  * New upstream version 3.30.6
  * Add myself to uploaders

  [ Andreas Tille ]
  * debhelper 10
  * d/watch: version=4

 -- Kevin Murray <kdmfoss@gmail.com>  Sun, 11 Dec 2016 12:02:11 +1100

r-bioc-limma (3.30.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Generic BioConductor homepage
  * Switch to dh-r

 -- Andreas Tille <tille@debian.org>  Fri, 28 Oct 2016 09:41:13 +0200

r-bioc-limma (3.28.17+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream version

 -- Gordon Ball <gordon@chronitis.net>  Thu, 28 Jul 2016 14:59:11 +0200

r-bioc-limma (3.28.10+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream version
  * Fix the run-unit-test script (don't check for exact stdout match)
    and add a missing test dependency (r-cran-mass)

 -- Gordon Ball <gordon@chronitis.net>  Tue, 21 Jun 2016 15:17:31 +0200

r-bioc-limma (3.28.4+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Use xz compression per d/watch file
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Thu, 19 May 2016 23:38:13 +0200

r-bioc-limma (3.26.1+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 02 Nov 2015 10:58:54 +0100

r-bioc-limma (3.24.15+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Fixed watch  file

 -- Andreas Tille <tille@debian.org>  Fri, 25 Sep 2015 22:00:43 +0200

r-bioc-limma (3.24.14+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 31 Jul 2015 19:07:56 +0200

r-bioc-limma (3.24.13+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 19 Jul 2015 18:14:45 +0200

r-bioc-limma (3.22.1+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 24 Oct 2014 23:22:11 +0200

r-bioc-limma (3.22.0+dfsg-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * Skip test needing GO.db which is not yet packaged

 -- Andreas Tille <tille@debian.org>  Thu, 16 Oct 2014 13:39:02 +0200

r-bioc-limma (3.20.9+dfsg-1) unstable; urgency=medium

  * New upstream version (adapted patch)
  * d/copyright: Updated copyright information

 -- Andreas Tille <tille@debian.org>  Tue, 16 Sep 2014 17:33:35 +0200

r-bioc-limma (3.20.7+dfsg-1) unstable; urgency=low

  [ Charles Plessy ]
  bff8c59 debian/control: removed myself from Uploaders.
  c3b46aa Imported Upstream version 3.18.10~dfsg

  [ Andreas Tille ]
  * New upstream version
  * Depends: r-cran-statmod
    Closes: #727693
  * Complies with Standards-Version: 3.9.5
  * d/copyright: check new version
  * d/control: Testsuite: autopkgtest

 -- Andreas Tille <tille@debian.org>  Fri, 27 Jun 2014 19:02:17 +0200

r-bioc-limma (3.16.7~dfsg-1) unstable; urgency=low

  be4e6a5 Imported Upstream version 3.16.7~dfsg
  5d10f8d Normalised VCS URLs.
  3ed3572 Signal the presence of autopkgtest suite with a XS-Testsuite field.

 -- Charles Plessy <plessy@debian.org>  Sun, 04 Aug 2013 17:28:41 +0900

r-bioc-limma (3.16.3~dfsg-1) unstable; urgency=low

  [ Charles Plessy ]
  ff8c6db Imported Upstream version 3.12.1~dfsg
  b4f097e Effective license is GPL-2, without the '+', as R is GPL-2.

  [ Andreas Tille ]
  * Imported new upstream version 3.16.3~dfsg
  * debian/control
     - added myself to Uploaders
     - Removed DM-Upload-Allowed
     - Standards-Version: 3.9.4 (no changes needed)
     - Debhelper 9
     - Fake normalisation as it is done by `cme fix dpkg-control`
       (the command did actually no change - the control file was
        brought into shape manually)
     - Removed versioned dependency of r-base-dev
  * debian/copyright:
     - s/Removed/Files-Excluded/ to comply with the suggested format
       that might be processed by uscan as alternative method to
       filter-pristine-tar
     - checked new upstream version and bumped version number
     - Add years to Copyright fields
  * debian/rules: Remove executable flag from doc files
  * debian/source/format: Add explicit source format specification

 -- Andreas Tille <tille@debian.org>  Thu, 16 May 2013 15:16:13 +0200

r-bioc-limma (3.12.0~dfsg-1) unstable; urgency=low

  [ Charles Plessy ]
  2d3420e Imported Upstream version 3.12.0~dfsg
  4bbb8e4 Document LIMMA's copyright as a whole.

  [ Andreas Tille ]
  8a61a4c Obtain references from inst/CITATION

 -- Charles Plessy <plessy@debian.org>  Thu, 12 Apr 2012 11:40:40 +0900

r-bioc-limma (3.10.3-1) unstable; urgency=low

  0ddb00d New Upstream version.

  [ Carlos Borroto ]
  afe6f31 Fix for watch file.

  [ Charles Plessy ]
  7ef2264 Removing inst/doc/usersguide.pdf, as its source is missing.
  4713d97 Obtain ${R:Depends} through r-base-dev.
  756a1da Conforms to Policy 3.9.3.
  68495e3 Renamed debian/upstream-metadata.yaml to debian/upstream.
  2db0d3d Verbose Debhelper.

 -- Charles Plessy <plessy@debian.org>  Wed, 07 Mar 2012 13:13:47 +0900

r-bioc-limma (3.10.0-1) unstable; urgency=low

  eebb062 New upstream release (Bioconductor 2.9).
  c1a8767 Include 7 digits of the commit ID in the changelog entry.

 -- Charles Plessy <plessy@debian.org>  Fri, 04 Nov 2011 13:35:49 +0900

r-bioc-limma (3.8.3-1) unstable; urgency=low

  7b5440e New upstream release.
  e891bb6 Corrected VCS-Git url.

 -- Charles Plessy <plessy@debian.org>  Sun, 14 Aug 2011 20:30:21 +0900

r-bioc-limma (3.8.2-1) unstable; urgency=low

  * Initial release (closes: #628548).

 -- Charles Plessy <plessy@debian.org>  Thu, 02 Jun 2011 13:07:58 +0900
